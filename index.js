function countLetter(letter, sentence) {
    if(letter.length == 1){ 
        for (let i = 0; i < sentence.length; i++){
            if (sentence[i] == letter){
              result += 1; 
            } 
        }return result; 

        } else {
        return undefined;
    }
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    }


function isIsogram(text) {
    return text.split('')
                .filter((item, pos, arr)=> arr
                .indexOf(item) == pos)
                .length == text.length;
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.   
}



function purchase(age, price) {
    if(age < 13){
        return undefined
    } else if (age >= 13 && age <= 21){
        price *= 0.8
    } else if (age >= 65){
        price *= 0.8
    } price = Math.round(price*100)/100
    return `${price}`
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    
}


function findHotCategories(items) {
    const categories = [];
    items.forEach((product) => {
        if (product.stocks === 0) {
            if (!categories.includes(product.category)){
                categories.push(product.category);
            }
        }
    })

     return categories;
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    // The passed items array from the test are the following:
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
}

   
function findFlyingVoters(candidateA, candidateB) {
    let voters = [];
    let voter = [];

    for(let voter of candidateA){
        if(candidateB.includes(voter)){
             voters.push(voter);
        }
    }
    return voters;
    
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};